package app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * This class represents the entry point of the application.
 * It loads the configuration file from an external
 * location or from the classpath and processes a message based on the configuration.
 * <p>
 * The configuration file is expected to be in a properties format
 * and should contain a 'name' key with a string value.
 * The message to be processed is constructed by concatenating the 'name' value with a string.
 * <p>
 * The output format of the message is determined by the 'type' system property,
 * which can be either 'xml' or 'json' value.
 * If 'xml' is specified, the message will be serialized to an XML format
 * using the XmlMapper class from
 * the Jackson library. Otherwise, it will be serialized to a JSON format
 * using the ObjectMapper class from the same library.
 * <p>
 * The processed message is logged at the INFO level using LogBack logging.
 */
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    private static final String CLASSPATH_CONFIG = "config.properties";
    private static final String EXTERNAL_CONFIG = "target/config/config.properties";
    private static final String TYPE = "type";
    private static final String NAME = "name";
    private static final String OUTPUT_XML = "xml";

    /**
     * This method loads the config file from an external location if it exists,
     * otherwise it loads from the classpath.
     * @param properties the Properties object to load the configuration properties into
     * @throws IOException if an error occurs while reading the configuration file
     */
    private void loadProperties(Properties properties) throws IOException {
        File externalConfigFile = new File(EXTERNAL_CONFIG);
        if (externalConfigFile.exists()) {

            try (FileInputStream file = new FileInputStream(externalConfigFile);
                 InputStreamReader reader = new InputStreamReader(file, StandardCharsets.UTF_8)) {
                properties.load(reader);
            } catch (IOException e) {
                LOGGER.error("Failed to load config from external location: {}", EXTERNAL_CONFIG, e);
            }
            LOGGER.info("Loaded config from external location: {}", EXTERNAL_CONFIG);
        } else {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(CLASSPATH_CONFIG);
            if (inputStream != null) {
                try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
                    properties.load(reader);
                } catch (IOException e) {
                    LOGGER.error("Failed to load config from classpath: {}", CLASSPATH_CONFIG, e);
                }
                LOGGER.info("Loaded config from classpath");
            } else {
                String errorMsg = "Config file not found.";
                LOGGER.error(errorMsg);
                throw new FileNotFoundException(errorMsg);
            }
        }
    }

    /**
     * This method converts a Message object to a String representation using the provided ObjectMapper.
     * @param message the Message object to be converted
     * @param mapper  the ObjectMapper to be used for the conversion
     * @return a String representation of the Message object
     * @throws IOException if an error occurs while converting the message to a string
     */
    private static String toMessage(Message message, ObjectMapper mapper) throws IOException {
        StringWriter writer = new StringWriter();
        mapper.writeValue(writer, message);
        return writer.toString();
    }

    /**
     * This method loads the configuration properties and runs the application.
     * If the external configuration file exists, it will be loaded from the external location.
     * Otherwise, it will be loaded from the classpath. The method then retrieves the message
     * name from the configuration file and constructs a Message object with it. Finally, it
     * converts the Message object to a String using either the XmlMapper or the ObjectMapper
     * (depending on the 'type' property), and logs the output.
     * @throws IOException if there is an error while loading the configuration file
     */
    public void run() {
        Properties properties = new Properties();
        try {
            loadProperties(properties);
            String type = System.getProperty(TYPE);
            Message msg = new Message(properties.getProperty(NAME));

            String output;
            if (OUTPUT_XML.equals(type)) {
                output = toMessage(msg, new XmlMapper());
            } else {
                output = toMessage(msg, new ObjectMapper());
            }
            LOGGER.info("Output: {}", output);
        } catch (IOException e) {
            LOGGER.error("An error occurred while processing the request", e);
        }
    }

    public static void main(String[] args) {
        new App().run();
    }
}