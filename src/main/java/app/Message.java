package app;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a message object containing a string message.
 * The message property is annotated with @JsonProperty to specify the JSON property name
 * in serialization and deserialization.
 */
public class Message {
    @JsonProperty(value = "message")
    private String message;

    /**
     * Constructs a Message object with a formatted string message.
     * @param message the string message to format and store in the message property
     */
    public Message(String message) {
        this.message = String.format("Привіт %s !", message);
    }
}

